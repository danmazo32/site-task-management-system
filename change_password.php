
<!DOCTYPE html>
<html>
<head>
 <title>Update Password</title>


       <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
       <link rel="stylesheet" type="text/css" href="css/iconfont/material-icons.css">
       <style type="text/css">
          .side-nav{
            top: 65px;
            width: 25%;
          }

          .material-icons.cDboard {color: #FFF !important;}

          @media only screen and (max-width: 600px) {
            .drag-target {
                top: 50px;
                width: 10% !important;
            }

            .side-nav {
                top: 57px;
            }
            
        }

          #nav {
            z-index: 10;
            position: relative;
          } 

          #sidenav-overlay {
            z-index: 9
          }
       </style>
       <meta charset="utf-8">
       <meta name="viewport" content="width=device-width, initial-scale=1.0" />
</head>
<body>
  <!-- Navbar Content -->
  <ul id="dropdown1" class="dropdown-content">
    <li><a href="profile.php"><i class="material-icons">account_circle</i>Profiles</a></li>
    <li><a href="login.php"><i class="material-icons">exit_to_app</i>Log out</a></li>
  </ul>
  <div class="navbar-fixed">
    <nav>
      <div class="nav-wrapper deep-purple darken-4">
        <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
        <a href="#!" class="flow-text">&nbspSite Task Management System</a>
        <ul id="nav-mobile" class="right hide-on-med-and-down">
          <li><a href="#!" class="dropdown-button" data-belowOrigin="true" data-activates="dropdown1">Username<i class="material-icons right">arrow_drop_down</i></a></li>
          <li><a href="#!"></a></li>
        </ul>
        <ul id="mobile-demo" class="side-nav deep-purple darken-5" style="width: 90% !important;">
          <li class="no-padding">
            <ul class="collapsible collapsible-accordion">
              <li>
                <a class="collapsible-header white-text"><i class="material-icons cDboard">account_box</i>Username<i class="material-icons cDboard right">arrow_drop_down</i></a>
                <div class="collapsible-body">
                  <ul>
                    <li><a href="profile.php"><i class="material-icons">account_circle</i>Profiles</a></li>
                    <li><a href="login.php"><i class="material-icons">exit_to_app</i>Log out</a></li>
                  </ul>
                </div>
              </li>
            </ul>
          </li>
          <li><a href="dashboard.php" class="white-text btn"><i class="material-icons cDboard  left">dashboard</i>Dashboard</a></li>
          <li><a href="users.php" class="white-text btn"><i class="material-icons cDboard left">supervisor_account</i>Accounts</a></li>
          <li><a href="tasks.php" class="white-text btn"><i class="material-icons cDboard left">assignment</i>Tasks</a></li>
          <li><a href="reminders.php" class="white-text btn"><i class="material-icons cDboard left">event_note</i>Reminders</a></li>
        </ul>
      </div>
    </nav>
  </div>

  <div class="row">
    <div class="col s3 m3 hide-on-med-and-down">
      <ul id="slide-out" class="side-nav fixed deep-purple darken-5 hide-on-med-and-down">
        <li><a href="dashboard.php" class="btn waves-effect waves-light deep-purple darken-3"><i class="material-icons cDboard  left">dashboard</i>Dashboard</a></li>
        <li><a href="users.php" class="btn waves-effect waves-light blue active"><i class="material-icons cDboard left">supervisor_account</i>Accounts</a></li>
        <li><a href="tasks.php" class="btn waves-effect waves-light deep-purple darken-3"><i class="material-icons cDboard left">assignment</i>Tasks</a></li>
        <li><a href="reminders.php" class="btn waves-effect waves-light deep-purple darken-3"><i class="material-icons cDboard left">event_note</i>Reminders</a></li>
      </ul>
    </div>
    <div class="col s12 m9">
      <h1>Update Password</h1>
      <hr>
      <div class="container">
       <ul class="collapsible popout">
            <li>
                <div class="collapsible-header #1e88e5 blue darken-1 active"><i class="material-icons">lock_outline</i>Password</div>
                <div class="collapsible-body">
                  <form>                    
                    <div class="input-field">
                      <input type="password" id="uname" class="validate" required>
                      <label for="uname" data-error="Required">New Password</label>
                    </div>                    
                    <div class="row">
                      <button type="submit" class="waves-effect waves-light btn #1976d2 blue darken-2"><i class="material-icons right">send</i>Update Password</button>
                      <a href="users.php" class="waves-effect waves-light btn #d32f2f red darken-2"><i class="material-icons right">cancel</i>Cancel</a>
                    </div>
                  </form>
               </div>
            </li>
        </ul>
    </div>
    </div>
  </div>

    
 
   <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
   <script type="text/javascript" src="js/materialize.min.js"></script>
   <script type="text/javascript">
     $(document).ready(function() {
      $(".button-collapse").sideNav();
      });
     
   </script>
</body>
</html>

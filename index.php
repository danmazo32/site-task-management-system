<!DOCTYPE HTML>
<html lang="en-US">
<head>
	<link rel="stylesheet" type="text/css" href="css/materialize.min.css">
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
	<title>SITE TASK MANAGEMENT SYSTEM - ONLINE BULLETIN BOARD</title>
	<style type="text/css">
		.card-panel {
			padding: 10px !important;
		}

		@media only screen and (max-width: 600px) {
            .header-info {
            	font-size: 20px;
            }
            
        }
	</style>
</head>
<body>
	<div class="row">
		<div class="col s12" style="background-color: yellow; height: 10px;"></div>
		<div class="col s12" style="background-color: green; height: 90px;">
			<div class="container">
				<div class="row valign-wrapper">
					<div class="col s2">
						<img class="responsive-img" src="img/image.png">
					</div>
					<div class="col s10">
						<h3 class="white-text left header-info">ST. PAUL UNIVERSITY PHILIPPINES</h3>
					</div>
					
				</div>	
				<div class="row">
					<div class="col s12">
						<h5>TASKS TO DO [SCHOOL OF INFORMATION TECHNOLOGY &amp ENGINEERING]</h5>
						<marquee behavior="alternate">
							<h5 class="blue-text">
								<?php
						          echo "Today is " . date("l") . " " . date("jS") . " of " . date("F Y");
						        ?>
							</h5>
						</marquee>
					</div>	
					<div class="row">
					<!-- Tasks -->
					<div class="col s12 m9">
						<div class="card-panel blue lighten-4">
							<h5>Insert Task title here</h5>
							<p>Person Concerned</p>
							<p>Status: &nbsp <span class="btn red">not yet started</span></p>
						</div>
						<div class="card-panel blue lighten-4">
							<h5><strong>THIS IS HOW TO MAKE A DASHBOARD, 2 DAYS OF NO CLASSES AND YOU GIVE ME A SHITTY OUTPUT!</strong></h5>
							<p>Dan Mikko Mazo</p>
							<p>Status: &nbsp <span class="btn green">Work in Progress</span></p>
						</div>
						<div class="card-panel blue lighten-4">
							<h5><strong>Attend Seminar</strong></h5>
							<p>Test User</p>
							<p>Status: &nbsp <span class="btn blue">Done</span></p>
						</div>
					</div>
					<div class="col s12 m3">
						<div class="card-panel green lighten-5">
							<h4 class="green-text">Reminders: </h4>
							<div class="divider"></div>
							<p>*10/19/17 Present Project</p>
							<div class="divider"></div>
							<p>*10/19/17 Give low scores during performance assessment</p>

						</div>
					</div>
				</div>
				</div>	
				
			</div>
		</div>
		
	</div>
	
</body>
</html>
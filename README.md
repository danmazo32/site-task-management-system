# README #

**Note: Work in your own Branches!, Do not Commit or Merge in the Master Branch**
**Should you be ready to Merge, Create a Pull Request and your code will be reviewed for merging**

### Chatroom ###


For Concerns, Help and Complaints
Gitter or Facebook, Gitter can be integrated with either your twitter or Github
[Gitter Chat](https://gitter.im/SITE-task-management-system-Team-ODD/Lobby)

### Tasks ###
Please refer to the Boards for the Task Assignment, For any concerns, Please discuss it in the Facebook Group Chatroom or Gitter

### Current Plan Implementation Status ###

- [ ] Interface Design, Using Materialize CSS for Front-End (Ongoing)
- [ ] Database Integration
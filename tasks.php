<!DOCTYPE html>
<html>
<head>
    <title>Tasks</title>
    <link rel="stylesheet" type="text/css" href="css/materialize.min.css">
    <link rel="stylesheet" type="text/css" href="css/iconfont/material-icons.css">
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <style type="text/css">
        .side-nav{
            top: 65px;
            width: 25%;
        }

        @media only screen and (max-width: 600px) {
            .drag-target {
                top: 50px;
                width: 10% !important;
            }

            .side-nav {
                top: 57px;
            }
            
        }

        .material-icons.cDboard {color: #FFF !important;}

        #nav {
          z-index: 10;
          position: relative;
        } 

        #sidenav-overlay {
          z-index: 9
        }

        li.taskCard:hover {

            box-shadow: 0 8px 10px 1px rgba(0,0,0,0.14), 0 3px 14px 2px rgba(0,0,0,0.12), 0 5px 5px -3px rgba(0,0,0,0.3);
        }
    </style>
</head>
<body>
    <ul id="dropdown1" class="dropdown-content">
        <li><a href="profile.php"><i class="material-icons">account_circle</i>Profiles</a></li>
        <li><a href="login.php"><i class="material-icons">exit_to_app</i>Log out</a></li>
    </ul>
    <div class="navbar-fixed">
        <nav>
            <div class="nav-wrapper deep-purple darken-4">
                <a href="#" data-activates="mobile-demo" class="button-collapse"><i class="material-icons">menu</i></a>
                <a href="#!" class="flow-text">&nbspSite Task Management System</a>
                <ul id="nav-mobile" class="right hide-on-med-and-down">
                    <li><a href="#!" class="dropdown-button" data-belowOrigin="true" data-activates="dropdown1">Username<i class="material-icons right">arrow_drop_down</i></a></li>
                    <li><a href="#!"></a></li>
                </ul>
                <ul id="mobile-demo" class="side-nav deep-purple darken-5" style="width: 90% !important;">
                    <li class="no-padding">
                        <ul class="collapsible collapsible-accordion">
                            <li>
                                <a class="collapsible-header white-text"><i class="material-icons cDboard">account_box</i>Username<i class="material-icons cDboard right">arrow_drop_down</i></a>
                                <div class="collapsible-body">
                                    <ul>
                                        <li><a href="profile.php"><i class="material-icons">account_circle</i>Profiles</a></li>
                                        <li><a href="login.php"><i class="material-icons">exit_to_app</i>Log out</a></li>
                                    </ul>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li><a href="dashboard.php" class="white-text btn"><i class="material-icons cDboard  left">dashboard</i>Dashboard</a></li>
                    <li><a href="users.php" class="white-text btn"><i class="material-icons cDboard left">supervisor_account</i>Accounts</a></li>
                    <li><a href="tasks.php" class="white-text btn"><i class="material-icons cDboard left">assignment</i>Tasks</a></li>
                    <li><a href="reminders.php" class="white-text btn"><i class="material-icons cDboard left">event_note</i>Reminders</a></li>
                </ul>
            </div>
        </nav>
    </div>

    <div class="row">
        <!-- Sidebar -->
        <div class="col s3 m3 hide-on-med-and-down">
            <ul id="slide-out" class="side-nav fixed deep-purple darken-5 hide-on-med-and-down">
                <li><a href="dashboard.php" class="btn waves-effect waves-light deep-purple darken-3"><i class="material-icons cDboard  left">dashboard</i>Dashboard</a></li>
                <li><a href="users.php" class="btn waves-effect waves-light deep-purple darken-3"><i class="material-icons cDboard left">supervisor_account</i>Accounts</a></li>
                <li><a href="tasks.php" class="btn waves-effect waves-light blue active"><i class="material-icons cDboard left">assignment</i>Tasks</a></li>
                <li><a href="reminders.php" class="btn waves-effect waves-light deep-purple darken-3"><i class="material-icons cDboard left">event_note</i>Reminders</a></li>
            </ul>
        </div>
        <div class="col s12 m9">
            <h1>Tasks</h1>
            <hr>
            <br>
            <div class="container">
                <!-- USER VIEW -->
                <ul class="collapsible">
            <li>
                <div class="collapsible-header active #1976d2 blue darken-2 white-text"><i class="material-icons">account_box</i>To-Do List</div>
                <div class="collapsible-body">
                    <table class="striped highlight responsive-table">
                        <thead>
                            <tr>
                                <th>Task Description</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Attend Campaign</td>
                                <td><a href="update_task.php" class="waves-effect waves-light btn #1976d2 blue darken-2">Start Task <i class="material-icons right">thumb_up</i></a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </li>
        </ul>

                <!-- ADMIN VIEW -->
        <a href="create_tasks.php" class="waves-effect waves-light btn #1976d2 blue darken-2">Create new task</a>
        <ul class="collapsible">
            <li>
                <div class="collapsible-header active #1976d2 blue darken-2 white-text"><i class="material-icons">assignment</i>Pending Tasks</div>
                <div class="collapsible-body">
                    <div class="row">
                        <ul class="collection sortable col s12">
                            <li class="collection-item card-panel blue lighten-4 taskCard">
                                <span class="title"><strong><i class="material-icons left">drag_handle</i>Attend Seminar</strong><a href="#!" class="secondary-content"><i class="material-icons">close</i></a></span>
                                <p>- Person Concerned  &nbsp <a href="update_task.php" class="btn-floating waves-effect waves-circle waves-light #1976d2 blue darken-2"><i class="material-icons ">edit</i></a></p>
                            </li>
                            <li class="collection-item card-panel blue lighten-4 taskCard">
                                <span class="title"><strong><i class="material-icons left">drag_handle</i>Finish the codes before midnight</strong><a href="#!" class="secondary-content"><i class="material-icons">close</i></a></span>
                                <p>- Dan Mazo &nbsp <a href="update_task.php" class="btn-floating waves-effect waves-circle waves-light #1976d2 blue darken-2"><i class="material-icons ">edit</i></a></p>
                            </li>
                            <li class="collection-item card-panel blue lighten-4 taskCard">
                                <span class="title"><strong><i class="material-icons left">drag_handle</i>Do the Hustle!</strong><a href="#!" class="secondary-content"><i class="material-icons">close</i></a></span>
                                <p>- Dan Mazo  &nbsp <a href="update_task.php" class="btn-floating waves-effect waves-circle waves-light #1976d2 blue darken-2"><i class="material-icons ">edit</i></a></p>
                            </li>
                            <li class="collection-item card-panel blue lighten-4 taskCard">
                                <span class="title"><strong><i class="material-icons left">drag_handle</i>Party Time!</strong><a href="#!" class="secondary-content"><i class="material-icons">close</i></a></span>
                                <p>- Dan Mazo  &nbsp <a href="update_task.php" class="btn-floating waves-effect waves-circle waves-light #1976d2 blue darken-2"><i class="material-icons ">edit</i></a></p>
                            </li>
                        </ul>                        
                    </div>
                </div>
            </li>
        </ul>
    </div>   
        </div>
    </div>

    
 
 
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <script type="text/javascript" src="js/materialize.min.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/ui/1.12.1/jquery-ui.min.js"></script>
    <!-- This hack works in mobiles -->
    <script type="text/javascript" src="js/jquery.ui.touch-punch.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function() {
            $(".button-collapse").sideNav();
            $( ".sortable" ).sortable({
                axis: 'y'
            });
        });
    </script>
</body>
</html>